import { Card, WingBlank, WhiteSpace } from 'antd-mobile';
import {useState,useEffect} from 'react'
import Item from 'antd-mobile/lib/popover/Item';


var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
today = mm + '/' + dd + '/' + yyyy;

const data = [
    {body:23,status:'order in today',date:today,image:'https://image.flaticon.com/icons/svg/3144/3144467.svg'},
    {body:32,status:'order out today',date:today,image:'https://image.flaticon.com/icons/svg/3176/3176179.svg'},
    {body:5324,status:'stock',date:'',image:'https://image.flaticon.com/icons/svg/2979/2979677.svg'},
    {body:533,status:'total in',date:'',image:'https://image.flaticon.com/icons/svg/3050/3050255.svg'},
    {body:67899,status:'total out',date:'',image:'https://image.flaticon.com/icons/svg/3050/3050234.svg'},
]

export default function Dashboard() {
    return (
        <div>
            { data.map((i,index) => { 
            return (
                 <div key={index}>
                    <WingBlank size="lg" >
                    <WhiteSpace size="lg" />
                    <Card >
                    <Card.Header
                        title={i.status}
                        thumb={i.image}
                        thumbStyle={{height:40,width:40}}
                        extra={<span>{i.date}</span>}
                    />
                    <Card.Body>
                        <div>
                        {i.body} items
                        </div>
                    </Card.Body>
                    </Card>
                    <WhiteSpace size="lg" />
                    </WingBlank>
                </div>
                )
            })}
        </div>
    )
}
