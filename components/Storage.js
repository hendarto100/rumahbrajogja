import { Card, WingBlank, WhiteSpace,SearchBar,List } from 'antd-mobile';
import {useState,useEffect} from 'react'
import Router from 'next/router'



var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
today = mm + '/' + dd + '/' + yyyy;

const Item = List.Item;
const Brief = Item.Brief;

const data = [
    {body:'Bra Young Hearts',status:'BYH010 GRY',date:today,image:'https://image.flaticon.com/icons/svg/3144/3144467.svg'},
    {body:'Bra Young Hearts',status:'BYH011 GRY',date:today,image:'https://image.flaticon.com/icons/svg/3176/3176179.svg'},
    {body:'Bra Young Hearts',status:'BYH012 GRY',date:'',image:'https://image.flaticon.com/icons/svg/2979/2979677.svg'},
    {body:'Bra Young Hearts',status:'BYH013 GRY',date:'',image:'https://image.flaticon.com/icons/svg/3050/3050255.svg'},
    {body:'Bra Young Hearts',status:'BYH014 GRY',date:'',image:'https://image.flaticon.com/icons/svg/3050/3050234.svg'},
]

export default function Storage() {

    const [width, setWidth] = useState(0)
    const [height, setHeight] = useState(0)

    useEffect(() => {
        
        setHeight(window.innerHeight)
        setWidth(window.innerWidth)
        console.log(`tingginya : ${height} dan lebarnya : ${width}`) 
        
    })
    return (
        <div>
            <SearchBar style={{zIndex:3,position:'fixed',width:'97.6%',padding:0}} placeholder="Search" maxLength={8}/>
            { data.map((i,index) => { 
            return (
                <div key={index} style={{paddingTop:30}}>
                <List >
                  <Item
                        onClick={() => {}}
                        platform="android">
                    <WhiteSpace size="lg" />
                    <Card full>
                      <Card.Header
                        title={i.status}
                        extra={<span>created {i.date}</span>}
                      />
                      <Card.Body>
                          <img src="https://instagram.fjog3-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s750x750/106923461_606496843608838_2126368525201919723_n.jpg?_nc_ht=instagram.fjog3-1.fna.fbcdn.net&_nc_cat=110&_nc_ohc=u6-OWUkuyCMAX9Ptlxo&oh=cc4ce162f0e1386be68356654c08ddc7&oe=5F30F1F5"
                          style={{width:"90%",height:"90%"}}/>
                        <div>{i.body}</div>
                        <a style={{fontStyle:'bold'}}>harga : Rp.55000,-/pcs</a>
                      </Card.Body>
                      <Card.Footer onClick={()=>{Router.push({pathname:'/EditItem',query:{status:i.status}})}}content="edit" extra={<div>45 items</div>} />
                    </Card>
                  </Item>
                </List>
              </div>
                )
            })}
        </div>
    )
}