// Config file
import * as firebase from "firebase";

const config = {
    apiKey: "AIzaSyB5hyrthOwttcvUTTBl0TGA4WfrF78CtCA",
    authDomain: "rumahbra-48ff6.firebaseapp.com",
    databaseURL: "https://rumahbra-48ff6.firebaseio.com",
    projectId: "rumahbra-48ff6",
    storageBucket: "rumahbra-48ff6.appspot.com",
    messagingSenderId: "578178133233",
    appId: "1:578178133233:web:5460c0740cfa8908534d50",
    measurementId: "G-TRM0FELZDP"
  };

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();

