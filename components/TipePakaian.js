import React from 'react'
import { Button,InputItem,WhiteSpace,Icon,Flex } from 'antd-mobile';
import {useState,useEffect} from 'react'


export default function TipePakaian() {
    const [loadingButton, setloadingButton] = useState(false)

    return (
        <div style={{backgroundColor:'white'}} >
            <Flex justify='center'>
            {/* <img style={{height:200,width:200}}src='https://ecs7.tokopedia.net/img/cache/700/product-1/2019/7/17/6344362/6344362_1cc46c30-20b7-4530-a037-097d08b45aae_640_640.jpg'/> */}
            </Flex>
            <WhiteSpace/>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="Tipe Pakaian">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/3126/3126709.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="contoh : s,m,l,xl / 34b,36c ...">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/2922/2922782.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="contoh : biru,merah,kuning">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/2919/2919740.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="Deskripsi" type="text">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/762/762702.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>
            
            <WhiteSpace/>

            <Button type="primary" 
            loading={loadingButton} onClick={()=>{setloadingButton(true)}}>Input</Button>
        </div>
    )
}
