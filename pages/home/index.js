import { Drawer, List, NavBar, Icon } from 'antd-mobile';
import Route from 'next/router'
import Dashboard from '../../components/Dashboard'
import Storage from '../../components/Storage'
import StockIn from '../../components/StockIn';
import TipePakaian from '../../components/TipePakaian';
import firebase from '../firebase';

var display ='block'

firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            console.log(user.email)
            
          } else {
            
            //alert('data kosong')
            Route.push('/login')
            // ...
          }
        });

export default class index extends React.Component {
    menus = [
            {icon : 'https://image.flaticon.com/icons/svg/2919/2919623.svg',name : 'Dashboard'},
            {icon : 'https://image.flaticon.com/icons/svg/715/715676.svg',name : 'Storage'},
            {icon : 'https://image.flaticon.com/icons/svg/3142/3142122.svg',name : 'Input'},
            {icon : 'https://image.flaticon.com/icons/svg/2916/2916103.svg',name : 'Output'},
            {icon : 'https://image.flaticon.com/icons/svg/3126/3126709.svg',name : 'Tipe Pakaian'},
            {icon : 'https://www.flaticon.com/premium-icon/icons/svg/3070/3070387.svg',name : 'Report'},
            {icon : 'https://image.flaticon.com/icons/svg/879/879393.svg',name : 'LogOut'},
            ]

    state = {
        docked: false,minHeight:'',content:'',display:'none'
    }

     
    componentDidMount(){
        this.setState({minHeight:document.documentElement.clientHeight})
        firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            display = 'block'
            console.log(user.email)
            
          } else {

            //alert('data kosong')
            Route.push('/login')
            // ...
          }
        });
    }

    onDock = (d) => {
        this.setState({
        [d]: !this.state[d],
        });
    }

    renderMenu = (index) => {
        switch (index) {
            case 0:
                this.setState({content:'Dasboard'})
                break;
            case 1:
                this.setState({content:'Storage'})
                break;
            case 2:
                this.setState({content:'Input'})  
                break;
            case 3:
                this.setState({content:'Output'})    
                break;
            case 4:
                this.setState({content:'Tipe Pakaian'})
                break;
            case 5:
                this.setState({content:'Report'})
                break;
            case 6:
                this.setState({content:'LogOut'})
                {
                        firebase.auth().signOut().then(function() {
                        Route.push('/login') // Sign-out successful. 
                      }).catch(function(error) {
                        alert(error)// An error happened.
                      });
                  }
                break;
            default:
                this.setState({content:'Dasboard'})
                break;
        }
    }

    contentRender = () => { 
        switch(this.state.content) {
            case 'Dasboard':
              return  <div> <Dashboard/> </div>
            case 'Storage':
              return  <div> <Storage/> </div>
            case 'Input':
              return  <div> <StockIn/> </div>
            case 'Output':
              return  <div> Menu Stock Out </div>
            case 'Tipe Pakaian':
                return <div> <TipePakaian/> </div>
            case 'LogOut':
                return <div></div>
            case 'Report':
                return <div> Menu Laporan </div>  
            default:
              return <div> <Dashboard/></div> 
          }
        }

  render() {
   
    const sidebar = (
    <List>
      {this.menus.map((i, index) => {
        return (<List.Item key={index}
          thumb={i.icon}
          onClick={() => this.renderMenu(index)}
        > {i.name}</List.Item>);
      })}
    </List>);

    return (<div style={{ height: '100%',display:display }}>
      <NavBar icon={<img src='https://image.flaticon.com/icons/svg/2919/2919540.svg' style={{height:30,width:30,}} alt=''/>}
       onLeftClick={() => this.onDock('docked')}>
        Rumah Bra Jogja
      </NavBar>
      <Drawer
        className="my-drawer"
        style={{ minHeight: this.state.minHeight-70 ,}}
        contentStyle={{ color: '#A6A6A6', textAlign: 'center', paddingTop: 0 }}
        sidebarStyle={{ border: '1px solid #ddd' }}
        sidebar={sidebar}
        docked={this.state.docked}
      >
        
        {this.contentRender()}
        {console.log('state sekarang :' + this.state.content)}
      </Drawer>
    </div>);
  }
}

