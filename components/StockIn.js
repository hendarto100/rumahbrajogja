import React from 'react'
import { Button,InputItem,WhiteSpace,Icon,Flex,ImagePicker,List,Stepper } from 'antd-mobile';
import {useState,useEffect} from 'react'

const Item = List.Item;

const data = []

const ukuran =['x','xl','xxl','s']
const warna = ['hijau','kuning','merah']

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

const tipePakaian = [{
    tipe_pakaian:"celana dalam",
    ukuran:"s,m,l,x,xl",
    warna:"biru,merah,kuning",
    deskripsi:"celana dalam perempuan"
},
{
    tipe_pakaian:"bra",
    ukuran:"34B,36c,37a",
    warna:"hitam",
    deskripsi:"bra perempuan"
},
]
export default function StockIn() {
    const [loadingButton, setloadingButton] = useState(false)
    const [files, setFiles] = useState(data)

    const onChange = (files, type, index) => {
        console.log(files, type, index);
        setFiles(files)
      }
    return (
        <div style={{backgroundColor:'white'}} >
            <Flex justify='center'>
            {/* <img style={{height:200,width:200}}src='https://ecs7.tokopedia.net/img/cache/700/product-1/2019/7/17/6344362/6344362_1cc46c30-20b7-4530-a037-097d08b45aae_640_640.jpg'/> */}
            </Flex>
            <WhiteSpace/>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="Kode item">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/3142/3142305.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="Nama">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/1828/1828492.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="Tipe Pakaian">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/3126/3126709.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="Merk">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/3050/3050241.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>

            <InputItem placeholder="Harga" type="money">
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/3190/3190615.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>
            <WhiteSpace/>

            <List renderHeader={() => 'Ukuran'}  >
                <Item thumb="https://image.flaticon.com/icons/svg/2922/2922782.svg">
                <select defaultValue="s" style={{fontSize:14,color:'gray'}}>
                {ukuran.map((i,index)=>{ return <option key={index} value={i}>{capitalize(i)}</option>})}
                </select>
                </Item>
            </List>
            <WhiteSpace/>
            <WhiteSpace/>
            

            <List renderHeader={() => 'Warna'}  >
                <Item thumb="https://image.flaticon.com/icons/svg/2919/2919740.svg">
                <select defaultValue="ungu" style={{fontSize:14,color:'gray'}}>
                    {warna.map((i,index)=>{ return <option key={index} value={i}>{capitalize(i)}</option>})}
                </select>
                </Item>
            </List>
            <WhiteSpace/>
            <WhiteSpace/>

            <Item thumb="https://image.flaticon.com/icons/svg/2670/2670607.svg"
            extra={<Stepper style={{ width: '100%', minWidth: '100px',fontSize:14 }} showNumber size="small" defaultValue={20} />} ><div style={{fontSize:15,color:'gray'}}>Jumlah item</div></Item>
            <WhiteSpace/>
            <WhiteSpace/>
            
            <WhiteSpace/>

            <ImagePicker
            files={files}
            onChange={onChange}
            onImageClick={(index, fs) => console.log(index, fs)}
            
            accept="image/gif,image/jpeg,image/jpg,image/png"
            />

            <Button type="primary" 
            loading={loadingButton} onClick={()=>{setloadingButton(true)}}>INPUT</Button>
        </div>
    )
}
