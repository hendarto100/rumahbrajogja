import React from 'react'
import { Button,InputItem,WhiteSpace,Icon,Flex } from 'antd-mobile';
import {useState,useEffect} from 'react'
import Head from 'next/head'
import firebase from '../firebase'
import  Router  from 'next/router';
export default function index() {
    const [loadingButton, setloadingButton] = useState(false)
    const [email, setEmail] = useState(0);
    const [password, setPassword] = useState(0)

    const firestore = firebase.firestore()
    const handleLogin = () => {
        firestore.collection("cities").doc("LA").set({
            name: "Los Angeles",
            state: "CA",
            country: "USA"
        })
        .then(function() {
            console.log("Document successfully written!");
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
        });

        setloadingButton(true)
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
        // Handle Errors here.
        
        var errorCode = error.code;
        var errorMessage = error.message;
        alert('login Gagal error code : '+ errorCode + ' message: '+errorMessage)
        // ...
        setloadingButton(false)
      });
    }
    
    useEffect(() => {
        
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
              // User is signed in.
              var displayName = user.displayName;
              var email = user.email;
              var emailVerified = user.emailVerified;
              var photoURL = user.photoURL;
              var isAnonymous = user.isAnonymous;
              var uid = user.uid;
              var providerData = user.providerData;
              if(!user.email){
                  Router.push('/login')
              }
              else{
                  Router.push('/home')
              }
              // ...
              
            } else {
             // alert('kosong')
              // ...
            }
          });
        
    }, [])



    return (
        <div style={{backgroundColor:'white'}} >
            <Head>
            <script src="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.js"></script>
            <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.css" />
            </Head>
            <Flex justify='center'>
            <img style={{height:200,width:200}}src='https://ecs7.tokopedia.net/img/cache/700/product-1/2019/7/17/6344362/6344362_1cc46c30-20b7-4530-a037-097d08b45aae_640_640.jpg'/>
            </Flex>
            <WhiteSpace/>
            <InputItem placeholder="input username" onChange={e => {setEmail(e)}} >
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/924/924874.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <InputItem type="password" placeholder="input password" onChange={e => {setPassword(e)}} >
            <div style={{ backgroundImage: 'url(https://image.flaticon.com/icons/svg/2913/2913133.svg)', backgroundSize: 'cover', height: '22px', width: '22px' }} />
            </InputItem>
            <WhiteSpace/>

            <Button type="ghost" 
            icon={<img src='https://image.flaticon.com/icons/svg/747/747504.svg' alt=''/>}
            loading={loadingButton} onClick={handleLogin}>Login</Button>
        </div>
    )
}
